#pragma once
#include "IDataAccess.h"
#include "sqlite3.h"
#include <io.h>
#include <sstream> 

#define DB_NAME "MyDB.sqlite"

class DatabaseAccess : public IDataAccess
{
public:
	DatabaseAccess() = default;
	virtual ~DatabaseAccess() = default;

	// album related
	const std::list<Album> getAlbums() override;
	const std::list<Album> getAlbumsOfUser(const User & user) override;
	void createAlbum(const Album & album) override;
	void deleteAlbum(const std::string & albumName, int userId) override;
	bool doesAlbumExists(const std::string & albumName, int userId) override;
	Album openAlbum(const std::string & albumName) override;
	void printAlbums() override;
	int getLastAlbumId() override;
	int getAlbumId(const std::string& albumName) override;

	// picture related
	void addPictureToAlbumByName(const std::string & albumName, const Picture & picture) override;
	void removePictureFromAlbumByName(const std::string & albumName, const std::string & pictureName) override;
	void tagUserInPicture(const std::string & albumName, const std::string & pictureName, int userId) override;
	void untagUserInPicture(const std::string & albumName, const std::string & pictureName, int userId) override;
	int getPictureId(const std::string& albumName, const std::string& pictureName) override;
	std::list<Picture> getPictures(const int albumId) override;
	std::list<Picture> getPicturesList() override;

	// user related
	void printUsers() override;
	void createUser(User & user) override;
	void deleteUser(const User & user) override;
	bool doesUserExists(int userId) override;
	User getUser(int userId) override;
	void getUsers() override;

	// user statistics
	int countAlbumsOwnedOfUser(const User & user) override;
	int countAlbumsTaggedOfUser(const User & user) override;
	int countTagsOfUser(const User & user) override;
	float averageTagsPerAlbumOfUser(const User & user) override;
	int getTagId() override;
	bool getTags(const int pictureId, const int userId) override;
	int countTagsOfUserInAlbum(const int userId, const int AlbumId) override;
	int getTaggedPictures(const int pictureId, const int userId) override;
	void printTags(const int pictureId, std::string pictureName) override;

	// queries
	User getTopTaggedUser() override;
	Picture getTopTaggedPicture() override;
	std::list<Picture> getTaggedPicturesOfUser(const User & user) override;

	// Callbacks
	static int deleteTagsCallback(void* data, int argc, char** argv, char** azColName);
	static int deletePicCallback(void* data, int argc, char** argv, char** azColName);
	static int getAlbumIdCallback(void* data, int argc, char** argv, char** azColName);
	static int getLastTagId(void* data, int argc, char** argv, char** azColName);
	static int getPictureIdCallback(void* data, int argc, char** argv, char** azColName);
	int deleteUserCallback(int userId);
	static int getLastAlbumIdCallback(void* data, int argc, char** argv, char** azColName);
	static int getAlbumsCallback(void* data, int argc, char** argv, char** azColName);
	static int CheckIfExistCallback(void* data, int argc, char** argv, char** azColName);
	static int getUserNameCallback(void* data, int argc, char** argv, char** azColName);
	static int getAlbumsOfUserCallback(void* data, int argc, char** argv, char** azColName);
	static int getPicturesCallback(void* data, int argc, char** argv, char** azColName);
	static int getNumOfTagsCallback(void* data, int argc, char** argv, char** azColName);
	static int CheckIfTaggedCallback(void* data, int argc, char** argv, char** azColName);
	static int getUsersCallback(void* data, int argc, char** argv, char** azColName);
	static int getLastUserIdCallback(void* data, int argc, char** argv, char** azColName);
	static int printTagsCallback(void* data, int argc, char** argv, char** azColName);
	static int getPicturesIdCallback(void* data, int argc, char** argv, char** azColName);

	bool open() override;
	void close() override;
	void clear() override;
};