#include "DatabaseAccess.h"

sqlite3* db;
bool Exist;
int AlbumId;
int LastUserId;
std::string UserName;
int TagId;
int AlbumLastId;
int NumOfAlbums;
int NumOfTags;
int NumOfTagsInPicture;
int UserIdToCheck;
int PicId;
std::list<Album> m_albums;
std::list<User> m_users;
std::list<Picture> m_pictures;
std::set<int> PicturesIdList;

int DatabaseAccess::deleteTagsCallback(void* data, int argc, char** argv, char** azColName) // Function deletes the tags from the picture.
{
	std::string picId = argv[0];
	std::string sqlStatement = "Delete FROM TAGS Where PICTURE_ID = ";
	sqlStatement += picId;
	sqlStatement += ";";

	char** errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, errMessage);
	//if (res != SQLITE_OK)
		//std::cout << "Failure!" << std::endl;

	return 0;
}

int DatabaseAccess::deletePicCallback(void* data, int argc, char** argv, char** azColName) // Function deletes the pictures from the album.
{
	std::string albumId = argv[0];
	std::string sqlStatement = "SELECT ID FROM PICTURES WHERE ALBUM_ID = ";
	char** errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), deleteTagsCallback, nullptr, errMessage);
	//if (res != SQLITE_OK)
		//std::cout << "Failure!" << std::endl;

	sqlStatement = "Delete FROM PICTURES Where ALBUM_ID = ";
	sqlStatement += albumId;
	sqlStatement += ";";

	errMessage = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failure!" << std::endl;

	return 0;
}

int DatabaseAccess::getAlbumIdCallback(void* data, int argc, char** argv, char** azColName) // Function gets the album id.
{
	std::string albumId = argv[0];
	std::stringstream id(albumId);
	id >> AlbumId;
	
	return 0;
}

int DatabaseAccess::getLastTagId(void* data, int argc, char** argv, char** azColName) // Function gets the last tag in the db.
{
	std::string tagId = argv[0];
	std::stringstream Id(tagId);
	Id >> TagId;
	TagId++;

	return 0;
}

int DatabaseAccess::getPictureIdCallback(void* data, int argc, char** argv, char** azColName) // Function gets picture id.
{
	std::string picId = argv[0];
	std::stringstream Id(picId);
	Id >> PicId;

	return 0;
}

int DatabaseAccess::deleteUserCallback(int userId) // Function deletes the user from the db.
{
	std::string sqlStatement = "SELECT ID FROM ALBUMS WHERE USER_ID = ";
	sqlStatement += std::to_string(userId);
	sqlStatement += ";";

	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), deletePicCallback, nullptr, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failure!" << std::endl;

	sqlStatement = "DELETE FROM ALBUMS WHERE USER_ID = ";
	sqlStatement += std::to_string(userId);
	sqlStatement += ";";

	errMessage = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failure!" << std::endl;
	
	return 0;
}

int DatabaseAccess::getLastAlbumIdCallback(void* data, int argc, char** argv, char** azColName) // Function gets the last album id.
{
	std::string albumId = argv[0];
	std::stringstream id(albumId);
	id >> AlbumLastId;
	AlbumLastId++;

	return 0;
}

int DatabaseAccess::getAlbumsCallback(void* data, int argc, char** argv, char** azColName) // Function gets list of all albums from db.
{
	Album album;
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "ID")
		{
		}
		else if (std::string(azColName[i]) == "NAME")
		{
			album.setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "CREATION_DATE")
		{
			album.setCreationDate(argv[i]);
		}
		else if (std::string(azColName[i]) == "USER_ID")
		{
			album.setOwner(atoi(argv[i]));
		}
	}
	m_albums.push_back(album);

	return 0;
}

int DatabaseAccess::CheckIfExistCallback(void* data, int argc, char** argv, char** azColName) // Function checks if this object is exist in db.
{
	std::string answer = argv[0];
	if (answer.empty())
	{
		Exist = false;
	}
	else
	{
		Exist = true;
	}

	return 0;
}

int DatabaseAccess::getUserNameCallback(void* data, int argc, char** argv, char** azColName) // Function gets the username.
{
	UserName = argv[0];
	return 0;
}

int DatabaseAccess::getAlbumsOfUserCallback(void* data, int argc, char** argv, char** azColName) // Function gets the number of albums for user.
{
	std::string albumsNumber = argv[0];
	std::stringstream num(albumsNumber);
	num >> NumOfAlbums;

	return 0;
}

int DatabaseAccess::getPicturesCallback(void* data, int argc, char** argv, char** azColName) // Function gets pictures from db.
{
	Picture pic;
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "ID")
		{
			pic.setId(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME")
		{
			pic.setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "LOCATION")
		{
			pic.setPath(argv[i]);
		}
		else if (std::string(azColName[i]) == "CREATION_DATE")
		{
			pic.setCreationDate(argv[i]);
		}
		else if (std::string(azColName[i]) == "ALBUM_ID")
		{
		}
	}
	m_pictures.push_back(pic);

	return 0;
}

int DatabaseAccess::getNumOfTagsCallback(void* data, int argc, char** argv, char** azColName) // Function gets the number of tags.
{
	std::string TagsNumber = argv[0];
	std::stringstream num(TagsNumber);
	num >> NumOfTags;

	return 0;
}

int DatabaseAccess::CheckIfTaggedCallback(void* data, int argc, char** argv, char** azColName) // Function gets the number of tags that user has in picture.
{
	NumOfTagsInPicture = 0;
	for (int i = 0; i < argc; i++)
	{
		if (UserIdToCheck == atoi(argv[i]))
		{
			NumOfTagsInPicture++;
		}
	}

	return 0;
}

int DatabaseAccess::getUsersCallback(void* data, int argc, char** argv, char** azColName) // Function gets the list of users from db.
{
	User user;
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "ID")
		{
			user.setId(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME")
		{
			user.setName(argv[i]);
		}
	}
	m_users.push_back(user);

	return 0;
}

int DatabaseAccess::getLastUserIdCallback(void* data, int argc, char** argv, char** azColName) // Function gets the last users id from db.
{
	if (argv[0])
	{
		std::string IdNumber = argv[0];
		std::stringstream id(IdNumber);
		id >> LastUserId;
		LastUserId++;
	}
	else
	{
		LastUserId = 1;
	}

	return 0;
}

int DatabaseAccess::printTagsCallback(void* data, int argc, char** argv, char** azColName) // Function prints the list of the tags.
{
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "ID")
		{
			std::cout << argv[i] << ", ";
		}
		else if (std::string(azColName[i]) == "PICTURE_ID")
		{
			std::cout << argv[i] << ", ";
		}
		else if (std::string(azColName[i]) == "USER_ID")
		{
			std::cout << argv[i] << "." << std::endl;
		}
	}

	return 0;
}

int DatabaseAccess::getPicturesIdCallback(void* data, int argc, char** argv, char** azColName) // Function gets the pictures id list from db.
{
	int picId;
	std::string IdNumber = argv[0];
	std::stringstream id(IdNumber);
	id >> picId;
	PicturesIdList.insert(picId);

	return 0;
}

bool DatabaseAccess::open() // Function opens the db.
{
	std::string dbFileName = DB_NAME;

	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &db);
	if (res != SQLITE_OK)
	{
		db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		return false;
	}
	if (doesFileExist == 0)
	{
		char* sqlStatement = "CREATE TABLE USERS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, NAME TEXT NOT NULL);";
		char** errMessage = nullptr;
		res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, errMessage);
		if (res != SQLITE_OK)
			return false;

		sqlStatement = "CREATE TABLE ALBUMS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, NAME TEXT NOT NULL, CREATION_DATE TEXT NOT NULL, USER_ID INTEGER NOT NULL);";
		errMessage = nullptr;
		res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, errMessage);
		if (res != SQLITE_OK)
			return false;
		
		sqlStatement = "CREATE TABLE PICTURES (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, NAME TEXT NOT NULL, LOCATION TEXT NOT NULL, CREATION_DATE TEXT NOT NULL, ALBUM_ID INTEGER NOT NULL);";
		errMessage = nullptr;
		res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, errMessage);
		if (res != SQLITE_OK)
			return false;
		
		sqlStatement = "CREATE TABLE TAGS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, PICTURE_ID INTEGER NOT NULL, USER_ID INTEGER NOT NULL);";
		errMessage = nullptr;
		res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, errMessage);
		if (res != SQLITE_OK)
			return false;
	}
	
	return true;
}

void DatabaseAccess::close() // Function closes the db.
{
	sqlite3_close(db);
	db = nullptr;
}

void DatabaseAccess::clear() // Function clears the lists that used in the code.
{
	m_users.clear();
	m_albums.clear();
	m_pictures.clear();
	PicturesIdList.clear();
	close();
}

const std::list<Album> DatabaseAccess::getAlbums() // Function gets the albums list from db.
{
	m_albums.clear();
	char* sqlStatement = "SELECT * FROM ALBUMS;";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement, getAlbumsCallback, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Error!" << std::endl;
	}

	return m_albums;
}

const std::list<Album> DatabaseAccess::getAlbumsOfUser(const User& user) // Function gets the albums list of user from db.
{
	m_albums.clear();
	std::string sqlStatement = "SELECT * FROM ALBUMS WHERE USER_ID = ";
	sqlStatement += std::to_string(user.getId());
	sqlStatement += ";";
	char** errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), getAlbumsCallback, nullptr, errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Error!" << std::endl;
	}
	
	return m_albums;
}

void DatabaseAccess::createAlbum(const Album& album) // Function creates album.
{
	this->getLastAlbumId();
	if (AlbumLastId == 0)
	{
		AlbumLastId = 1;
	}
	std::string sqlStatement = "Insert into ALBUMS VALUES (";
	sqlStatement += std::to_string(AlbumLastId);
	sqlStatement += ", ";
	sqlStatement += '"' + album.getName() + '"';
	sqlStatement += ", ";
	sqlStatement += '"' + album.getCreationDate() + '"';
	sqlStatement += ", ";
	sqlStatement += std::to_string(album.getOwnerId());
	sqlStatement += ");";

	char** errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failure!" << std::endl;
}

void DatabaseAccess::deleteAlbum(const std::string& albumName, int userId) // Function deletes album from db.
{
	int albumId = this->getAlbumId(albumName);

	std::string sqlStatement = "Delete FROM ALBUMS Where USER_ID = ";
	sqlStatement += std::to_string(userId);
	sqlStatement += " AND ID = ";
	sqlStatement += std::to_string(albumId);
	sqlStatement += ";";

	char** errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, errMessage);
	//if (res != SQLITE_OK)
		//std::cout << "Failure!" << std::endl;
}

bool DatabaseAccess::doesAlbumExists(const std::string& albumName, int userId) // Function checks if album exist in db.
{
	Exist = false;
	std::string sqlStatement = "SELECT ID FROM ALBUMS WHERE USER_ID = ";
	sqlStatement += std::to_string(userId);
	sqlStatement += " AND NAME = ";
	sqlStatement += '"' + albumName + '"';
	sqlStatement += ";";

	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), CheckIfExistCallback, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Error!" << std::endl;
		return false;
	}

	return Exist;
}

Album DatabaseAccess::openAlbum(const std::string& albumName) // Function opens the album from the db.
{
	std::list<Album> albums = this->getAlbums();
	
	for (auto& album : albums)
	{
		if (albumName == album.getName())
		{
			return album;
		}
	}
	std::cout << "No album with name " + albumName + " exists" << std::endl;
	
	return Album();
}

void DatabaseAccess::printAlbums() // Function prints all albums.
{
	m_albums.clear();
	const std::list<Album> albums = this->getAlbums();
	auto iter = albums.begin();
	while (iter != albums.end())
	{
		std::cout << iter->getName() << " , "
			<< iter->getCreationDate() << " , "
			<< iter->getOwnerId() << std::endl;
		++iter;
	}
}

int DatabaseAccess::getLastAlbumId() // Function gets last album id.
{
	std::string sqlStatement = "SELECT ID From ALBUMS Order By ID desc LIMIT 1;";

	char** errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), getLastAlbumIdCallback, nullptr, errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Failure!" << std::endl;
		return -1;
	}

	return 0;
}

int DatabaseAccess::getAlbumId(const std::string& albumName) // Function gets the album id by its name.
{
	std::string sqlStatement = "SELECT Distinct ID FROM ALBUMS WHERE NAME = ";
	sqlStatement += '"' + albumName + '"' + ";";
	char** errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), getAlbumIdCallback, nullptr, errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failure!" << std::endl;

	return AlbumId;
}

void DatabaseAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture) // Function adds picture to album by its name.
{
	int albumId = this->getAlbumId(albumName);

	std::string sqlStatement = "Insert into PICTURES VALUES (";
	sqlStatement += std::to_string(picture.getId());
	sqlStatement += ", ";
	sqlStatement += '"' + picture.getName() + '"';
	sqlStatement += ", ";
	sqlStatement += '"' + picture.getPath() + '"';
	sqlStatement += ", ";
	sqlStatement += '"' + picture.getCreationDate() + '"';
	sqlStatement += ", ";
	sqlStatement += std::to_string(albumId);
	sqlStatement += ");";

	char** errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failure!" << std::endl;
}

void DatabaseAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName) // Function removes picture from album by the picture name.
{
	std::string sqlStatement = "SELECT Distinct ID FROM PICTURES WHERE NAME = ";
	sqlStatement += '"' + pictureName + '"' + ";";
	char** errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), deleteTagsCallback, nullptr, errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failure!" << std::endl;

	int albumId = this->getAlbumId(albumName);
	sqlStatement = "Delete FROM PICTURES Where NAME = ";
	sqlStatement += '"' + pictureName + '"';
	sqlStatement += " AND ALBUM_ID = ";
	sqlStatement += std::to_string(albumId) + ";";

	errMessage = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failure!" << std::endl;
}

void DatabaseAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId) // Function tags a user in picture.
{
	int picId = this->getPictureId(albumName, pictureName);
	int tagId = this->getTagId();

	if (picId != -1 && tagId != -1)
	{
		std::string sqlStatement = "Insert into TAGS VALUES (";
		sqlStatement += std::to_string(tagId);
		sqlStatement += ", ";
		sqlStatement += std::to_string(picId);
		sqlStatement += ", ";
		sqlStatement += std::to_string(userId);
		sqlStatement += ");";

		char** errMessage = nullptr;
		int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, errMessage);
		if (res != SQLITE_OK)
			std::cout << "Failure!" << std::endl;
	}
}

void DatabaseAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId) // Function deletes tag from picture.
{
	int picId = this->getPictureId(albumName, pictureName);
	if (picId != -1)
	{
		std::string sqlStatement = "Delete FROM TAGS Where PICTURE_ID = ";
		sqlStatement += std::to_string(picId);
		sqlStatement += " And USER_ID = ";
		sqlStatement += std::to_string(userId) + ";";

		char** errMessage = nullptr;
		int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, errMessage);
		if (res != SQLITE_OK)
			std::cout << "Failure!" << std::endl;
	}
}

int DatabaseAccess::getPictureId(const std::string& albumName, const std::string& pictureName) // Function gets the picture id by picture name and album name.
{
	int albumId = this->getAlbumId(albumName);

	std::string sqlStatement = "SELECT Distinct ID FROM PICTURES WHERE NAME = ";
	sqlStatement += '"' + pictureName + '"';
	sqlStatement += " And ALBUM_ID = ";
	sqlStatement += std::to_string(albumId) + ';';
	
	char** errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), getPictureIdCallback, nullptr, errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Failure!" << std::endl;
		return -1;
	}
	
	return PicId;
}

std::list<Picture> DatabaseAccess::getPictures(const int albumId) // Function gets the pictures list from album.
{
	m_pictures.clear();
	std::string sqlStatement = "SELECT * FROM PICTURES WHERE ALBUM_ID = ";
	sqlStatement += std::to_string(albumId) + ";";

	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), getPicturesCallback, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Error!" << std::endl;
	}

	return m_pictures;
}

std::list<Picture> DatabaseAccess::getPicturesList() // Function gets all pictures from db.
{
	m_pictures.clear();
	std::string sqlStatement = "SELECT * FROM PICTURES;";

	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), getPicturesCallback, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Error!" << std::endl;
	}

	return m_pictures;
}

void DatabaseAccess::printUsers() // Function prints all the users.
{
	this->getUsers();
	auto iter = m_users.begin();
	while (iter != m_users.end())
	{
		std::cout << iter->getId() << " , " << iter->getName() << std::endl;
		++iter;
	}
}

void DatabaseAccess::createUser(User& user) // Function creates user and insert it into db.
{
	std::string sqlStatement = "SELECT MAX(ID) FROM USERS;";
	
	char** errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), getLastUserIdCallback, nullptr, errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failure!" << std::endl;

	sqlStatement = "INSERT INTO USERS VALUES (";
	sqlStatement += std::to_string(LastUserId);
	sqlStatement += ",";
	sqlStatement += '"' + user.getName() + '"';
	sqlStatement += ");";

	errMessage = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failure!" << std::endl;
}

void DatabaseAccess::deleteUser(const User& user) // Function deletes user from db.
{
	this->deleteUserCallback(user.getId());

	std::string sqlStatement = "Delete FROM USERS Where ID = ";
	sqlStatement += std::to_string(user.getId()) + ";";

	char** errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failure!" << std::endl;
}

bool DatabaseAccess::doesUserExists(int userId) // Function checks if user is exist in the db.
{
	Exist = false;
	std::string sqlStatement = "SELECT ID FROM USERS WHERE ID = ";
	sqlStatement += std::to_string(userId) + ";";

	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), CheckIfExistCallback, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Error!" << std::endl;
		return false;
	}

	return Exist;
}

User DatabaseAccess::getUser(int userId) // Function gets user by its id from the db.
{
	std::string sqlStatement = "SELECT NAME FROM USERS WHERE ID = ";
	sqlStatement += std::to_string(userId) + ";";
	char** errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), getUserNameCallback, nullptr, errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failure!" << std::endl;

	User user(userId, UserName);
	
	return user;
}

void DatabaseAccess::getUsers() // Function gets all users from the db.
{
	m_users.clear();
	std::string sqlStatement = "SELECT * FROM USERS;";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), getUsersCallback, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Error!" << std::endl;
	}
}

int DatabaseAccess::countAlbumsOwnedOfUser(const User& user) // Function that gets the number of albums that a user have.
{
	std::string sqlStatement = "SELECT COUNT(ID) FROM ALBUMS WHERE USER_ID = ";
	sqlStatement += std::to_string(user.getId()) + ";";

	char** errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), getAlbumsOfUserCallback, nullptr, errMessage);
	if (res != SQLITE_OK)
		std::cout << "Failure!" << std::endl;

	return NumOfAlbums;
}

int DatabaseAccess::countAlbumsTaggedOfUser(const User& user) // Function gets the number of albums that a user tagged in.
{
	int albumsCount = 0;

	std::list<Album> albums = this->getAlbums();

	for (const auto& album : albums)
	{
		int albumId = this->getAlbumId(album.getName());
		const std::list<Picture>& pics = this->getPictures(albumId);

		for (const auto& picture : pics)
		{
			bool isTag = this->getTags(picture.getId(), user.getId());
			if (isTag)
			{
				albumsCount++;
				break;
			}
		}
	}

	return albumsCount;
}

int DatabaseAccess::countTagsOfUser(const User& user) // Function gets the number of tags that user have in all pictures.
{
	std::string sqlStatement = "SELECT COUNT(ID) From TAGS WHERE USER_ID = ";
	sqlStatement += std::to_string(user.getId()) + ";";

	char** errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), getNumOfTagsCallback, nullptr, errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Failure!" << std::endl;
		return -1;
	}

	return NumOfTags;
}

float DatabaseAccess::averageTagsPerAlbumOfUser(const User& user) // Function gets the average number of tags that user has in all albums.
{
	float avg = 0;
	int countTags = 0;
	std::list<Album> albums = this->getAlbums();
	
	for (auto i = albums.begin(); i != albums.end(); i++)
	{
		int albumId = this->getAlbumId(i->getName());
		avg += this->countTagsOfUserInAlbum(user.getId(), albumId);
		countTags++;
	}

	return avg / countTags;
}

int DatabaseAccess::getTagId() // Function gets the last tag id from the db.
{
	std::string sqlStatement = "SELECT ID From TAGS Order By ID desc LIMIT 1;";

	char** errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), getLastTagId, nullptr, errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Failure!" << std::endl;
		return -1;
	}

	if (TagId == 0)
	{
		TagId = 1;
	}

	return TagId;
}

bool DatabaseAccess::getTags(const int pictureId, const int userId) // Function checks if tag exist in the db.
{
	Exist = false;
	std::string sqlStatement = "SELECT ID From TAGS WHERE PICTURE_ID = ";
	sqlStatement += std::to_string(pictureId);
	sqlStatement += " AND USER_ID = ";
	sqlStatement += std::to_string(userId) + ";";

	char** errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), CheckIfExistCallback, nullptr, errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Failure!" << std::endl;
		return false;
	}

	return Exist;
}

int DatabaseAccess::countTagsOfUserInAlbum(const int userId, const int albumId) // Function gets the number of tags that user has in album.
{
	int countTags = 0;
	const std::list<Picture>& pics = this->getPictures(albumId);

	for (const auto& picture : pics)
	{
		countTags += this->getTaggedPictures(picture.getId(), userId);
	}

	return countTags;
}

int DatabaseAccess::getTaggedPictures(const int pictureId, const int userId) // Function gets the number of tags that in picture.
{
	UserIdToCheck = userId;
	std::string sqlStatement = "SELECT USER_ID From TAGS WHERE PICTURE_ID = ";
	sqlStatement += std::to_string(pictureId) + ";";

	char** errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), CheckIfTaggedCallback, nullptr, errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Failure!" << std::endl;
		return 0;
	}

	return NumOfTagsInPicture;
}

void DatabaseAccess::printTags(const int pictureId, std::string pictureName) // Function prints the tags that tagged in picture.
{
	std::string sqlStatement = "SELECT * From TAGS WHERE PICTURE_ID = ";
	sqlStatement += std::to_string(pictureId) + ";";

	std::cout << "Tagged users in picture <" << pictureName << "> :" << std::endl;

	char** errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), printTagsCallback, nullptr, errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Failure!" << std::endl;
	}
}

User DatabaseAccess::getTopTaggedUser()  // Function gets the top tagged user.
{
	this->getUsers();
	int topTags = 0;
	int currAmountOfTags = 0;
	User topUser;

	for (const auto& user : m_users)
	{
		currAmountOfTags = this->countTagsOfUser(user);
		if (currAmountOfTags >= topTags)
		{
			topTags = currAmountOfTags;
			topUser = user;
		}
	}

	return topUser;
}

Picture DatabaseAccess::getTopTaggedPicture() // Function gets top tagged picture.
{
	std::list<Picture> picList = this->getPicturesList();
	int topTags = 0;
	Picture topPicture;

	for (const auto& picture : picList)
	{
		std::string sqlStatement = "SELECT COUNT(ID) FROM TAGS WHERE PICTURE_ID = ";
		sqlStatement += std::to_string(picture.getId()) + ";";

		char** errMessage = nullptr;
		int res = sqlite3_exec(db, sqlStatement.c_str(), getNumOfTagsCallback, nullptr, errMessage);
		if (res != SQLITE_OK)
		{
			std::cout << "Failure!" << std::endl;
		}

		if (NumOfTags >= topTags)
		{
			topTags = NumOfTags;
			topPicture = picture;
		}
	}

	return topPicture;
}

std::list<Picture> DatabaseAccess::getTaggedPicturesOfUser(const User& user) // Function gets the pictures id that the user owned.
{
	m_pictures.clear();
	std::list<Picture> picturesList = this->getPicturesList();
	std::list<Picture> returnList;

	std::string sqlStatement = "SELECT PICTURE_ID From TAGS WHERE USER_ID = ";
	sqlStatement += std::to_string(user.getId()) + ";";

	char** errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), getPicturesIdCallback, nullptr, errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Failure!" << std::endl;
	}

	for (const auto& picture : picturesList)
	{
		for (const auto& picId : PicturesIdList)
		{
			if (picture.getId() == picId)
			{
				returnList.push_back(picture);
			}
		}
	}

	return returnList;
}
